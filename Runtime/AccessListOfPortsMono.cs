using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AccessListOfPortsMono : MonoBehaviour
{
    public List<string> m_portsList;
    public uint[] m_portsIdList;

    void Start()
    {
        RefreshPortList();
    }

    [ContextMenu("Refresh List")]
    public void RefreshPortList()
    {
        m_portsList = SerialPortUtility.GetListOfAvailaiblePort();
        m_portsIdList = SerialPortUtility.GetSerialPortId();
    }


    public void Update()
    {

        m_portsList.AddRange(SerialPortUtility.GetListOfAvailaiblePort());
    }
}
